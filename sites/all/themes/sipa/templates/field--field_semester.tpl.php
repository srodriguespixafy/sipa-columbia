<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>&nbsp;</div>
  <?php endif; ?>
  <?php
    foreach ($items as $delta => $item) {
      switch ($item['#markup']) {
        case 'x':
            $item['#markup'] = "Autumn";
            break;
        case 'y':
            $item['#markup'] = "Spring";
            break;
        case 'z':
            $item['#markup'] = "Summer";
            break;
        case 'x and y':
            $item['#markup'] = "Autumn and Spring";
            break;
        case 'x or y':
            $item['#markup'] = "Autumn or Spring";
            break;
      }
      print render($item);    
    }
  ?>
</div>
