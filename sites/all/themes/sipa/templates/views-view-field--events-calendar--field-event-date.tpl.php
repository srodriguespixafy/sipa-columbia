<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php
    $date_value = new DateTime($row->_field_data['nid']['entity']->field_event_date['und'][0]['value']);
    $date_value2 = new DateTime($row->_field_data['nid']['entity']->field_event_date['und'][0]['value2']);
    
    $event_date = $date_value->format('F d, Y');
    if ( isset($date_value2) && $date_value2 != null && $date_value2->format('F d, Y') != $date_value->format('F d, Y')) {
        $event_date .= " - ".$date_value2->format('F d, Y');
    }
    $event_time = $date_value->format('g:ia');
    if ( isset($date_value2) && $date_value2 != null && $date_value2 != $date_value) {
        $event_time .= " to ".$date_value2->format('g:ia');
    }
    
?>    
<div class="date"><?php print $event_date;?></div>
<div class="time"><?php print $event_time;?></div>
