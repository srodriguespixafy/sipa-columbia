<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>&nbsp;</div>
  <?php endif; ?>
    <?php foreach ($items as $delta => $item): ?>
      <?php if ($element['#items'][$delta]['value']) : ?>
        <div class="on_leave">On Leave</div>
      <?php endif; ?>
    <?php endforeach; ?>
</div>
