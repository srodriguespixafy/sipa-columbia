<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<?php
  $first_result = reset($result);
  $rendered_course = $first_result->field_field_associated_course[0]['rendered'];
?>
<div <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
  <div class="field-group-format group_course field-group-div ">
    <?php print render($rendered_course['field_total_credits']); ?>
    <?php print render($rendered_course['field_course_number']);?>
    <?php print render($rendered_course['title']); ?>
    <?php print render($rendered_course['field_semester']); ?>
  </div>
  <div class="field-group-format field-group-div group-course-exp collapsible">
    <h3><span class="field-group-format-toggler"></span></h3>
    <div class="field-group-format-wrapper">
      <?php print render($rendered_course['field_course_description']); ?>
    
      <table>
        <?php if (!empty($header)) : ?>
          <thead>
            <tr>
              <?php foreach ($header as $field => $label): ?>
                <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
                  <?php print $label; ?>
                </th>
              <?php endforeach; ?>
            </tr>
          </thead>
        <?php endif; ?>
        <tbody>
          <?php foreach ($rows as $row_count => $row): ?>
            <tr class="<?php print implode(' ', $row_classes[$row_count]); ?>">
              <?php foreach ($row as $field => $content): ?>
                <td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
                  <?php print $content; ?>
                </td>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>