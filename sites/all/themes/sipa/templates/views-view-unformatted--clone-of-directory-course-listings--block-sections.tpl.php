<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php 
    $class_group = array();
    $instr = array();
    for ($id=0; $id<sizeof($view->result); $id++) {
        $class_id=$view->result[$id]->nid;
        $class_group[$class_id][] = $id;
    }
    foreach ($class_group as $group) {
        $first_id = reset($group);
        foreach ($group as $id) {
            foreach ($view->result[$id]->field_field_course_instructor as $instr_pos => $instr) {
                if ($instr['raw']['tid'] == $view->result[$id]->field_field_uni_instructor_link[0]['raw']['tid']) {
                    $instr[$first_id]['name'] = $view->result[$id]->field_uni_instructor_link_taxonomy_term_data_title;
                    $instr[$first_id]['nid'] = $view->result[$id]->field_uni_instructor_link_taxonomy_term_data_nid;
                    print 'found';
                }
                print $instr['raw']['tid'].'not'. $view->result[$id]->field_field_uni_instructor_link[0]['raw']['tid'].'<br>';
            }
            if ($id != $first_id) {
                unset($view->result[$id]);
            }
        }
    }
    
?>
<?php foreach ($view->result as $id => $row): ?>
  <div class="<?php print $classes_array[$id]; ?>">
     <?php 
     if (empty($instr[$id])) {
          print 'vid '.$row->field_field_course_instructor['rendered']['title'];
     }
     else {
         print 'Name '.t($instr[$id]['name']);
     }
    ?>     
 </div>
<?php endforeach; ?>
