<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>&nbsp;</div>
  <?php endif; ?>
    <?php
    foreach ($items as $delta => $item) {
      if ($element['#view_mode']=='teaser') {
        $start_date = new DateTime($element['#items'][$delta]['value']);
        $end_date = new DateTime($element['#items'][$delta]['value2']);
        $now = new DateTime();
        if ($now > $start_date && $now < $end_date) {
          print t("Active");
        }
        else {
          print t("Not Active");
        }
      }
      else {
        print render($item);
      }
    }
    ?>
</div>
