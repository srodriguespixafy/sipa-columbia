<?php
  $deadline = new DateTime($element['#object']->field_application_deadline['und'][0]['value']);
  $now = new DateTime();
  $open = false;
  if ($deadline < $now) {
    $classes .= " open allowed-open";    
    $open = true;
  }
  else {
    $classes .= " open";
    $open = true;
  }
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>&nbsp;</div>
  <?php endif; ?>
    <?php foreach ($items as $delta => $item): ?>
      <?php if ($open) : ?>
      <?php print render($item); ?>
</div>
      <?php else: ?>
      <div>Apply Now</div>
</div>
<p>Application Closed!</p>
      <?php endif; ?>
    <?php endforeach; ?>
