<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>




<div class="views-field field-hidden photo">        
  <div class="field-content photo"></div>  
</div>

<?php if($row['ldap_uni']):?>
<div class="field field-uni"><?php print $row['ldap_uni']; ?></div>
<?php endif; ?>
<?php if($row['ldap_Title']):?>
<div class="field field-title"><?php print $row['ldap_Title']; ?></div>
<?php endif; ?>
<?php if($row['ldap_ou']):?>
<div class="field field-department"><?php print str_replace("^"," ",$row['ldap_ou']); ?></div>
<?php endif; ?>
<?php if($row['ldap_campusphone']):?>
<div class="field field-phone"><?php print $row['ldap_campusphone']; ?></div>
<?php endif; ?>
<?php if($row['ldap_facsimiletelephonenumber']):?>
<div class="field field-fax"><?php print $row['ldap_facsimiletelephonenumber']; ?></div>
<?php endif; ?>
<?php if($row['ldap_postaladdress']):?>
<div class="field field-office-address"><?php print str_replace("$","	",$row['ldap_postaladdress']); ?></div>
<?php endif; ?>
<?php if($row['ldap_mail']):?>
<div class="field field-email"><?php print l($row['ldap_mail'],'mailto:'.$row['ldap_mail']); ?></div>
<?php endif; ?>

<div class="views-field field-hidden graduation_year">        
  <div class="field-content graduation_year">Graduation Year: </div>  
</div>

<div class="views-field field-hidden degree_program">        
  <div class="field-content degree_program">Degree Program: </div>  
</div>

<div class="views-field field-hidden field_of_study">        
  <div class="field-content field_of_study">Field Of Study: </div>  
</div>

<div class="views-field field-hidden specializations">        
  <div class="field-content specializations">Specialization(s): </div>  
</div>

<div class="views-field field-hidden cocurricular">        
  <div class="field-content cocurricular">Co-Curricular: </div>  
</div>

<div class="views-field field-hidden faculty_bio">        
  <div class="field-content faculty_bio"></div>  
</div>


<?php $profile_info = get_contact_profile_info_by_uni($row['ldap_uni'],'student_contact'); 
	if(!$profile_info){
		$profile_info = get_contact_profile_info_by_uni($row['ldap_uni'],'staff_contact');
	}
?>

<div class="field field-name-node-link field-type-ds field-label-hidden">
           <?php print l('View Details','node/'.$profile_info[0]->nid); ?>               
</div>