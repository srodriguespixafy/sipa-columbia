<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<?php 
switch($row->field_field_course_offered_unify[0]['rendered']['#markup']){
  case 'Y':
	break;
  case 'N':
  	$current_year_node = node_load(19151);
  	$year_info = node_load($current_year_node->field_current_year['und'][0]['nid']);
    $offered_text = "Not Offered in ".$year_info->title.". ".(user_access('edit any current_academic_year_unify content')?l('(Edit Current Year) ','node/19151/edit',array('attributes'=>array('target'=>'_blank'),'query'=>array('destination'=>$_GET['q']))):'');
  break;
  
}
$offered_text = "<span class='offered-text'>".$offered_text."</span>";

$output = $row->field_field_prerequisites[0]['rendered']['#markup'] ? '<strong>Prerequisites: </strong>' . $row->field_field_prerequisites[0]['rendered']['#markup'] . '<br/>' . $output : $output;
/* // Remove per client request 
foreach($row->field_field_academic_courses as $k=>$v){
    $tmp[] = $v['rendered']['#markup'];
}
*/
print $offered_text . $output . (count($tmp) ? ' ' . implode(', ',$tmp) : '');
?>
