<?php

/**
 * @file
 * Display Suite 1 column template.
 *
 * Add class to identify presence/absence of photo
 * Hide 'Biography' label is no bio exists
 * Omit Education wrapper if no education credentials
 */
?>
<div class="<?php print $classes;?> clearfix <?php print $ds_content_classes;?> <?php print (empty($field_headshot)) ? 'no_photo' : 'photo'; ?>">
    <?php if (isset($title_suffix['contextual_links'])): ?>
        <?php print render($title_suffix['contextual_links']); ?>
    <?php endif; ?>

    <?php if (empty($body)): ?>
        <?php $content['group_bio_wrap']['#prefix'] =
            str_replace('<h2><span>Biography</span></h2>', '', $content['group_bio_wrap']['#prefix']); ?>
    <?php endif; ?>
    <?php if (empty($field_education_credential) && !$is_admin) : ?>
        <?php $content['group_bio_wrap']['group_wrapper1']['group_education_wrap'] = null;?>
    <?php endif; ?>
    <?php print render($content); ?>
</div>
