<?php

/**
 * @file
 * Display Suite 1 column template.
 */
?>
<?php $content['group_wrapper'] = str_replace('<div', '<div id="top_info"', $content['group_wrapper']); ?>
<div class="ds-1col <?php print $classes;?> clearfix <?php print $ds_content_classes;?>">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <?php print render($content); ?>
</div>