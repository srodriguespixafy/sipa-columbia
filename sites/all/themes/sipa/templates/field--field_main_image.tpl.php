<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>&nbsp;</div>
  <?php endif; ?>
  <?php if (empty($element['#object']->field_banner )) : ?>
    <?php drupal_add_js('jQuery(document).ready(function () {jQuery("body").removeClass("no-banner").addClass("banner");})', 'inline'); ?>
    <?php foreach ($items as $delta => $item): ?>
            <?php print render($item); ?>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
