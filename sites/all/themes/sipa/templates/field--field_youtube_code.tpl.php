<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
	<?php foreach ($items as $delta => $item): ?>
		<iframe width="620" height="349" src="<?php print ($_SERVER['HTTPS'] == 'on'?'https://':'http://');?>www.youtube.com/embed/<?php print render($item); ?>?wmode=transparent" frameborder="0" allowfullscreen></iframe>
	<?php endforeach; ?>
</div>