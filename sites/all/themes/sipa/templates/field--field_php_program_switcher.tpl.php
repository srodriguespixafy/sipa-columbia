<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>&nbsp;</div>
  <?php endif; ?>
  <?php if ($element['#items'][0]['value']): ?>     
    <?php
        
      $tab_menu_items = array (
        "MIA" => "master-of-international-affairs",
        "MPA" => "master-of-public-administration",
        "PEPM" => "program-in-economic-policy-management",
        "EMPA" => "executive-mpa",
        "ESP" => "mpa-in-environmental-science-and-policy",
        "MPA-DP" => "mpa-in-development-practice",
        "PhD" => "phd-in-sustainable-development"
      );   
    
    
      $page_uri = request_uri();
      $arg_start = strpos($page_uri, '?');

      if ($arg_start) {
          $view_path = substr($page_uri,0,$arg_start);
      }
      else {
          $view_path = $page_uri;
      }
    
      foreach ($tab_menu_items as $key => $path_element) {
        $view_path = str_replace('/'.$path_element,'',$view_path);
      }
            
      $args = "";
      if ($arg_start) {
          $args = substr($page_uri, $arg_start);
      }
    ?>
    <div class="program-tabs">
      <div class="wrapper">
        <ul class="view_tabs">
          <?php foreach ($tab_menu_items as $tag => $path_element) : ?>
            <li class="view_tab <?php if (strpos($page_uri, $path_element) ) { print 'active';} ?>"><a href="<?php print $view_path . '/'.$path_element.$args; ?>"><?php print $tag;?></a></li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  <?php endif; ?>

</div>
