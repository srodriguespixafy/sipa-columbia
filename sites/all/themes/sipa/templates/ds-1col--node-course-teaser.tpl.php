<?php

switch ($node->field_semester['und'][0]['taxonomy_term']->name) {
          case 'x':
              $semester = "Autumn";
              break;
          case 'y':
              $semester =  "Spring";
              break;
          case 'z':
              $semester =  "Summer";
              break;
          case 'x and y':
              $semester =  "Autumn and Spring";
              break;
          case 'x or y':
              $semester =  "Autumn or Spring";
              break;
         default:
            $semester = $node->field_semester['und'][0]['taxonomy_term']['name'];
        }
        
 $assoc_course_section = get_associated_course_section($node->nid);
 $assoc_course_more_info = $assoc_course_section[0]->field_field_more_info[0]['rendered']['#markup'];
 $assoc_course_more_info_link = l('View in Directory of Classes',$assoc_course_more_info,array('attributes'=>array('target'=>'_blank')));
 $assoc_course_more_info_link = "<div class='views-field-field-more-info'>".$assoc_course_more_info_link."</div>";

?>
<div class="group-course-info">
<div class="field field-name-field-course-number field-type-text field-label-hidden"> <?php print $node->field_course_number['und'][0]['value']; ?> </div>
<div class="field field-name-title field-type-ds field-label-hidden"> <?php print $node->title; ?>,</div>
<div class="field field-name-field-semester field-type-taxonomy-term-reference field-label-hidden"><?php print $semester; ?></div>
<?php print $assoc_course_more_info_link; ?> 
</div>