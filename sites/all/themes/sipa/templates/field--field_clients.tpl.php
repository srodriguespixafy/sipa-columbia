<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php 
  $node_page = arg(0) == 'node' && arg(1);
  $capstone_dir = arg(0) == 'academics' && arg(1) == 'capstone-workshops';
  $output = array();   
  $field_client = "";
  foreach ($items as $delta => $item): ?>
    <?php 
    $field_client = field_collection_item_load($element['#items'][$delta]['value']);
    $field_client_name = $field_client->field_client_name['und'][0]['value'];
    $field_client_website = $field_client->field_client_website['und'][0]['url'];
    if ($field_client_website && ($node_page || $capstone_dir)) {
      $output[] = l($field_client_name,$field_client_website,array('attributes'=>array('target'=>'_blank')));
    }
    elseif ($field_client_name) {
      $output[] = $field_client_name;	      
    }
    ?>
  <?php endforeach; ?>
  
  
  <?php if(count($output) > 0):?>
  
  <div class="entity entity-field-collection-item field-collection-item-field-clients clearfix">
  <div class="content">
  <div class="field field-name-field-client-name field-type-text field-label-hidden last first">
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label.($node_page?'':':')?>&nbsp;</div>
  <?php endif; ?>
  <?php print implode(', ',$output);
   
  ?>
  </div>
  </div>
  </div>
  <?php endif; ?>
</div>
