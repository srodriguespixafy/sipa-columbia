<?php
/**
 * @file views-view.tpl.php
 * Main view template
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>

<?php
/**
 * Set path including any arguments for 'view all' link
 */
  $page_uri = check_plain(request_uri());
  $arg_start = strpos($page_uri,'?');
  if ($arg_start) {
    $args = substr($page_uri,$arg_start);
  }
  else {
    $args = '';
  }
  $all_link = '/'.current_path().'/all'.$args;
  $header = str_replace('path_goes_here', $all_link, $header);
  $footer = str_replace('path_goes_here', $all_link, $footer);
?>

<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
 
  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

 <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>
  
  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>
  
  <?php
    $page_uri = request_uri();
    $arg_start = strpos($page_uri, '?');

    $view_path_end = strpos($page_uri, '/', 1);
    if ($view_path_end) {
        $view_path = substr($page_uri,0,$view_path_end);
    }
    else {
        if ($arg_start) {
            $view_path = substr($page_uri,0,$arg_start);
        }
        else {
            $view_path = $page_uri;
        }
        
    }
    $args = "#top_info";
    if ($arg_start) {
        $args = substr($page_uri, $arg_start);
    }
?>
    <div id="content_top" class="wrapper">
    <ul class="view_tabs">
        <li class="view_tab first <?php if (strpos($page_uri, 'news_center_article')) { print 'active';} ?>"><a href="<?php print $view_path . '/news-center/news_center_article/' . $args; ?>">Articles</a></li>
        <li class="view_tab <?php if (strpos($page_uri, 'video')) { print 'active';} ?>"><a href="<?php print $view_path . '/news-center/video/' . $args; ?>">Videos</a></li>
        <li class="view_tab last <?php if (strpos($page_uri, 'photo_gallery')) { print 'active';} ?>"><a href="<?php print $view_path . '/news-center/photo_gallery/' . $args; ?>">Galleries</a></li>
        <li class="view_tab last <?php if (strpos($page_uri, 'in_the_news')) { print 'active';} ?>"><a href="<?php print $view_path . '/news-center/in_the_news/' . $args; ?>">In the News</a></li>
    </ul>
    </div>
  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>


  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
<?php /* drupal_add_js('jQuery(document).ready(function () {jQuery("div.region-content-inner div.feed-icon").insertAfter(jQuery("div.views-exposed-widget.views-widget-sort-by"))})','inline'); */ ?>
