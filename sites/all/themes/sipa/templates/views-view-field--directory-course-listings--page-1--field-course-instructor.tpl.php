<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php if (!empty($row->field_uni_instructor_link_taxonomy_term_data_nid)) : ?>
    <a href="/<?php print drupal_get_path_alias('node/'.$row->field_uni_instructor_link_taxonomy_term_data_nid); ?>">
<?php endif; ?>    
        <?php print_r($row->field_field_course_instructor[0]['rendered']['#title']); ?>
<?php if (!empty($row->field_uni_instructor_link_taxonomy_term_data_nid)) : ?>
    </a>
<?php endif; ?>    
