<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>&nbsp;</div>
  <?php endif; ?>
    <?php foreach ($items as $delta => $item): ?>
    <div class="event-buttons">
      <div class="button add-to-calendar">
        <a href="http://calendar.columbia.edu/sundial/webapi/iCalendar.php?EventID=<?php print $element['#items'][$delta]['value']; ?>">Add To Calendar</a>
      </div>
      <div class="button register">
        <?php if($element['#object']->field_rsvp_link['und'][0]['value']){
        
                  print $element['#object']->field_rsvp_link['und'][0]['value'];
              }
        ?>
      </div>
    </div>
    <?php endforeach; ?>
</div>
