<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
/**
 * Replace uni instructor names with link to SIPA faculty page if possible
 */
?>
<table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
  <?php endif; ?>
  <?php 
    $class_group = array(); // array of rows grouped by nid
    $skip_row = array();   // array of duplicate rows to skip
    
    // Group rows by course section nid
    for ($id=0; $id<sizeof($view->result); $id++) {
        $class_id=$view->result[$id]->nid;
        $class_group[$class_id][] = $id;
    }
    
    foreach ($class_group as $group) { // group is an array of one or more rows with the same nid
        $first_rownum = reset($group);  // get the first row id for this nid
        foreach ($group as $rownum) {
            // for each instructor in this row
            
            //This stuff has been moved into the new field_instructor_bio field and the template for field_field_course_instructor will handle the display
            
            /*foreach ($view->result[$rownum]->field_field_course_instructor as $instr_pos => $instr) {
                // if the term id matches the term id in the related faculty node
                if ($instr['raw']['tid'] == $view->result[$rownum]->field_field_uni_instructor_link[0]['raw']['tid']) {
                    // create a link for the related faculty node
                    $instr_name = $view->result[$rownum]->field_uni_instructor_link_taxonomy_term_data_title;
                    $instr_nid = $view->result[$rownum]->field_uni_instructor_link_taxonomy_term_data_nid;
                    $new_instr = l($instr_name, 'node/'.$instr_nid);
                    
                    // replace the old instructor name with the link 
                    $rows[$first_rownum]['field_course_instructor'] = str_replace($instr['rendered']['#markup'], $new_instr, $rows[$first_rownum]['field_course_instructor']);
                }
                
            }*/
            // if this isn't the first row for this course section nid, it's a duplicate
            if ($rownum != $first_rownum) {
                $skip_row[] = $rownum;
            }
        }
    }
    
?>
  <?php if (!empty($header)) : ?>
    <thead>
      <tr>
        <?php foreach ($header as $field => $label): ?>
          <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
            <?php print $label; ?>
          </th>
        <?php endforeach; ?>
      </tr>
    </thead>
  <?php endif; ?>
  <tbody>
    <?php foreach ($rows as $row_count => $row): ?>
    <?php if (in_array($row_count, $skip_row)) { continue;} // skip duplicates ?>
      <tr class="<?php print implode(' ', $row_classes[$row_count]); ?>">
        <?php foreach ($row as $field => $content): ?>
          <td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
            <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
