<div class="ds-1col <?php print $classes;?> clearfix <?php print $ds_content_classes;?>">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

<?php foreach ($elements as $key => $element): ?>
<?php if(substr($key,0,1) != '#' && key_exists(0, $element) && key_exists('#markup', $element[0])): ?>
    <?php if ($key == "title") : ?>
        <?php
        $doc_title = $title;
        if (sizeof($node->field_document_type) > 0) {
            $doc_title .= ' ('.$node->field_document_type['und'][0]['safe_value'].')';
        }

        $title_href = "";
        if (sizeof($node->field_pdf) > 0) {
            $title_href = file_create_url($node->field_pdf['und'][0]['uri']);
            //$title_href = str_replace('public://','/sites/default/files/',$title_href);
        }
        elseif (sizeof($node->field_research_link) > 0) {

            $title_href = $node->field_research_link['und'][0]['display_url'];
        }
        ?>
        <?php if ($title_href != "") : ?>
            <div class="faculty_research_title"><a href="<?php print $title_href; ?>" target="_blank"><?php print $doc_title; ?></a></div>
        <?php else: ?>
            <div class="faculty_research_title"><?php print $title; ?></div>
        <?php endif; ?>
    <?php else: ?>
    <?php print render($element);?>
    <?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>
  
  <?php //print $ds_content; ?>
</div>