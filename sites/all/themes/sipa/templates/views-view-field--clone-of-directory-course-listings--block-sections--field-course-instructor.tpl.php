<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<?php 

if($output == 'Instructor To Be Announced'){
	$output = 'Recitation: <br>Instructor TBA';
}else{
	if($row->field_field_instructor_bio){
		foreach($row->field_field_instructor_bio as $key => $instructor_bio){
			$instructor_arr[] = l($instructor_bio['rendered']['#title'],$instructor_bio['rendered']['#href']);
		}
		$output = implode(',',$instructor_arr);
	}else{
		if($row->field_field_course_instructor){
			foreach($row->field_field_course_instructor as $key => $instructor_bio){
				$instructor_arr[] = $instructor_bio['raw']['taxonomy_term']->name;
			}
			$output = implode(',',$instructor_arr);			
		}
	}
}

print $output;
?>