<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 
 /**
  * Print header fields (always visible and content fields (hidden when node is  
  * collapsed) in separate divs
  */
?>
<?php $course_content_ids = array('field_course_description', 'field_academic_courses', 'view'); ?>
<div class="wrap">
    <div class="group_course">
        <?php foreach ($fields as $id => $field): ?>
            <?php if (in_array($id, $course_content_ids)) { continue; } ?>
            <?php
				if (!empty($field->separator)): 
				print $field->separator; 
				endif; 
				
				print $field->wrapper_prefix; 
				print $field->label_html; 
				print $field->content; 
				print $field->wrapper_suffix;
?>
        <?php endforeach; ?>
    </div>
    <div class="field-group-format">
      <h3><span class="toggler">trigger</span></h3>
      <div class="course_content">
        <?php foreach ($fields as $id => $field): ?>
            <?php if (!in_array($id, $course_content_ids)) { continue; } ?>
            <?php
				if (!empty($field->separator)): 
				print $field->separator; 
				endif; 
				
				print $field->wrapper_prefix; 
				print $field->label_html; 
				print $field->content; 
				print $field->wrapper_suffix;
            ?>
        <?php endforeach; ?>
      </div>
    </div>
</div>