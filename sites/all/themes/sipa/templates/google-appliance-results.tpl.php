<?php
// $Id$
/**
 * @file 
 *    default theme implementation for displaying Google Search Appliance results
 *
 * This template collects each invocation of theme_google_appliance_result(). This and
 * the child template are dependent on one another sharing the markup for the 
 * results listing
 *
 * @see template_preprocess_google_appliance_results()
 * @see template_preprocess_google_appliance_result()
 * @see google-appliance-result.tpl.php
 */
//dsm($variables);
?>

<div class="group_top_info">
	<h1 id="search-results-heading"><?php print $results_heading; ?></h1>
	<div class="">
		<?php print drupal_render($search_form); ?>
	</div>
	 <?php if ($show_synonyms) : ?>
	  <div class="synonyms google-appliance-synonyms">
	    <span class="p"><?php print $synonyms_label ?></span> <ul><?php print $synonyms; ?></ul>
	  </div>
	 <?php endif; ?>
</div>

<div class="wrapper_full">
	<?php if (!isset($response_data['error'])) : ?>
	  
	  <div class="wrapper google-appliance-results-control-bar clearfix">
	    <div class="result_count"><?php print $search_stats; ?></div>
	    <div class="sort_by"><?php print $sort_headers; ?></div>
	  </div>
	
	  <ol class="wrapper keymatch-results google-appliance-keymatch-results">
	    <?php print $keymatch_results; ?>
	  </ol>
	
	  <ol class="wrapper search-results google-appliance-results">
	    <?php print $search_results; ?>
	  </ol>
	  
	  <div class="wrapper google-appliance-results-control-bar2 clearfix">
	    <div class="result_count"><?php print $search_stats; ?></div>
	    <div class="sort_by"><?php print $sort_headers; ?></div>
	  </div>
	  
	  <?php print $pager; ?>
	
	
		<?php else : ?>
		<div id="no-result" class="wrapper_small">
		  <?php print $error_reason ?>
		 </div>
		<?php endif; ?>
	
</div>
