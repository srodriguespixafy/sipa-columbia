<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <input type="checkbox" />
  <?php if (!$label_hidden): ?>
    <label class="field-label"<?php print $title_attributes; ?>><?php print $label ?>&nbsp;</label>
  <?php endif; ?>
</div>
