<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

function sipa_alpha_preprocess_html(&$variables) {
  $variables['attributes_array']['class'][] = 'no-banner';
//  drupal_add_js('http://fast.fonts.com/jsapi/72cc6303-ec35-4952-90db-0ec71b0e5384.js', 'external');

/*
  $js = '
  var sNew = document.createElement("script");
  sNew.async = true;
  sNew.src = "http://fast.fonts.com/jsapi/72cc6303-ec35-4952-90db-0ec71b0e5384.js";
  var s0 = document.getElementsByTagName("script")[0];
  s0.parentNode.insertBefore(sNew, s0);
  ';  
  drupal_add_js($js, array('type' => 'inline'));
*/
	  $script = '
		<!--[if IE]>
	  <script type="text/javascript">
	  	if(navigator.appVersion.indexOf("MSIE 8")==-1 && document.documentMode == "7"){
				alert("Your browser is running in compatibility mode. Please turn off compatibility mode to view the site properly.");
		}
	  </script>
	  <![endif]-->';
	$element = array(
	  '#type' => 'markup',
	  '#markup' => $script,
	  '#weight' => '-99999');
  drupal_add_html_head($element, 'IE8compatfix');


	$ieFix = array(
	  '#tag' => 'script',
	  '#attributes' => array( // Set up an array of attributes inside the tag
	    'src' => ($_SERVER['HTTPS']?'https':'http').'://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js', 
	  ),
	  '#prefix' => '<!--[if lt IE 9]>',
	  '#suffix' => '</script><![endif]-->',
	);
	drupal_add_html_head($ieFix, 'ieFix');
	
	if(arg(0) == 'node' && arg(1) == '18551' && !arg(2)){
		//page not found ,set meta refresh to redirect to homepage
				
		$redirect_script = '
      <script type="text/javascript">
      	setTimeout("location.href=\''.url('<front>',array('absolute'=>1)).'\'", 5000);
      </script>';
    $element = array(
      '#type' => 'markup',
      '#markup' => $redirect_script,
    );
    drupal_add_html_head($element, 'jquery-tmpl');

	}
	
	


  drupal_add_css(($_SERVER['HTTPS']?'https':'http').'://fast.fonts.com/cssapi/72cc6303-ec35-4952-90db-0ec71b0e5384.css', array('group' => CSS_THEME, 'type' => 'external'));

  // Conditional stylesheets for IE
  drupal_add_css(drupal_get_path('theme' , 'sipa') . '/css/ie-lte-8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE, 'weight' => 115));
  drupal_add_css(drupal_get_path('theme' , 'sipa') . '/css/ie-lte-7.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE, 'weight' => 115));
  drupal_add_css(drupal_get_path('theme' , 'sipa') . '/css/ie-6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE, 'weight' => 115));

  $meta = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=10,chrome=1',
    ),
    '#weight' => '-99998',
  );
  drupal_add_html_head($meta, 'iecompat');
}



function sipa_alpha_preprocess_node(&$variables) {
  global $user;
  if ($variables['page']) {
    if (isset($variables['field_require_wind']['und'][0]['value'])) { /* per-node WIND authentication */
      if ($variables['field_require_wind']['und'][0]['value'] == 1 && $user->uid == 0) {
        drupal_goto("user/wind", array('query' => array('wind_destination_path' => 'node/'. $variables['nid'])));
      }
    }
  }

	switch($variables['node']->type){
		//case'student_contact':
		case'staff_contact':
				$profile_info = get_contact_profile_info_by_uni($variables['node']->field_uni['und'][0]['value'],$variables['node']->type);				
				$ldap_info = get_contact_ldap_info_by_uni($variables['node']->field_uni['und'][0]['value'],'results');
				
				if($profile_info[1]->field_field_headshot['0']['rendered']['#item']['uri']){
					$photo = theme('image_style', array('style_name'=>'faculty_directory_thumb','path' => $profile_info[1]->field_field_headshot['0']['rendered']['#item']['uri']));
					$variables['photo_profile'] = "<div class='views-field field-type-image photo'><div class='field-content photo'>".$photo."</div></div>";
				}elseif($profile_info[0]->field_field_photo['0']['rendered']['#item']['uri']){
					$photo = theme('image_style', array('style_name'=>'faculty_directory_thumb','path' => $profile_info[0]->field_field_photo['0']['rendered']['#item']['uri']));
					$variables['photo_profile'] = "<div class='views-field field-type-image photo'><div class='field-content photo'>".$photo."</div></div>";
				}
	
				$department_ldap = $variables['node']->field_contact_department['und'][0]['value']?$variables['node']->field_contact_department['und'][0]['value']:$ldap_info[0]['ldap_ou'];
				if($department_ldap){
					$variables['department_ldap'] = "<div class='field field-department'>".$department_ldap."</div>";
				}
			
				$title_ldap = $variables['node']->field_contact_title['und'][0]['value']?$variables['node']->field_contact_title['und'][0]['value']:$ldap_info[0]['ldap_Title'];
				if($title_ldap){
					$variables['title_ldap'] = "<div class='field field-title'>".$title_ldap."</div>";
				}
				
				$email_ldap = $variables['node']->field_email['und'][0]['email']?$variables['node']->field_email['und'][0]['email']:$ldap_info[0]['ldap_mail'];
				if($email_ldap){
					$variables['email_ldap'] = "<div class='field field-name-field-email field-type-email field_email_ldap'>".l($email_ldap, 'mailto:'.$email_ldap, array('absolute' => TRUE))."</div>";;
				}
				
				$phone_number_ldap = $variables['node']->field_phone['und'][0]['value']?$variables['node']->field_phone['und'][0]['value']:$ldap_info[0]['ldap_telephonenumber'];
				if($phone_number_ldap){
					$variables['phone_number_ldap'] = "<div class='field field-name-field-phone field_phone_number_ldap'>".$phone_number_ldap."</div>";
				}
				
				$office_address_ldap = str_replace("$"," ",($variables['node']->field_address['und'][0]['value']?$variables['node']->field_address['und'][0]['value']:$ldap_info[0]['ldap_postaladdress']));
				if($office_address_ldap){
					$variables['office_address_ldap'] = "<div class='field field-name-field-address field-type-text-long field_office_address_ldap'>".$office_address_ldap."</div>";
				}
				
				/*
				
				//leave this for the student and the faculty contact pages if needed at some point for reference il2189
				if($ldap_info[0])
				{						
					if($variables['node']->field_uni['und'][0]['value']||$ldap_info[0]['ldap_uni']){
						$variables['uni_ldap'] = "<div class='field field-name-field-uni field-type-uni field_uni_ldap'>".($variables['node']->field_uni['und'][0]['value']?$variables['node']->field_uni['und'][0]['value']:$ldap_info[0]['ldap_uni'])."</div>";		
					}
					
										
					if($ldap_info[0]['ldap_facsimiletelephonenumber']){
						$variables['fax_ldap'] = "<div class='field field-fax'>".$ldap_info[0]['ldap_facsimiletelephonenumber']."</div>";
					}
				
				}*/
		
				/*
				$title = "<div class='field field-name-title'><h2>".$variables['title']."</h2></div>";
				
				$department_term = taxonomy_term_load($variables['field_administrative_department']['und'][0]['tid']);
				if($department_term){
					$department = "<div class='field field-name-field-administrative-department field-type-text-long'>".$department_term->name."</div>";
				}
				
				if($variables['field_faculty_bio']['und'][0]['nid']){
					$faculty_bio_node = node_load($variables['field_faculty_bio']['und'][0]['nid']);
					if($faculty_bio_node){
						$faculty_bio = $faculty_bio_node->body['und'][0]['value'];
					}
				}
				
				$graduation_year = $variables['field_graduation_year']['und'][0]['value'];
				
				if($graduation_year){
					$graduation_year = "<div class='field field-graduation-year'>Graduation Year: ".$graduation_year."</div>";
				}
				
				
				if($variables['node']->type == 'student_contact'){
					unset($variables['office_address_ldap']);
				}
				
				$variables['ldap_info'] = $title.$variables['photo_profile'].$variables['uni_ldap'].$variables['title_ldap'].$variables['department_ldap'].$variables['fax_ldap'].$variables['phone_number_ldap'].$variables['office_address_ldap'].$variables['email_ldap'];
				$variables['ldap_info'] = "<div class='wrapper_small group_contact_wrap clearfix'><div class='group_contact_info'>".$variables['ldap_info'].$graduation_year.$department.$faculty_bio."</div></div>";
				*/
				
		break;
		
		case 'course':
		case 'course_section':
			if(arg(0) == 'node' && arg(1) && !arg(2)){
				//check one more time
				$page_node = node_load(arg(1));
				if($page_node->type == 'course' || $page_node->type == 'course_section'){
					drupal_goto($variables['node']->field_more_info['und'][0]['url']?$variables['node']->field_more_info['und'][0]['url']:'<front>');
				}
			}
		break;
		
	}

}



function sipa_alpha_preprocess_views_view(&$vars) {

  $view = $vars['view'];
  if ($view->name == 'directory_people_from_ldap') {  
    drupal_add_js(drupal_get_path('module', 'sipa_helper') . '/js/sipa_helper_profile_data.js');
  }
}