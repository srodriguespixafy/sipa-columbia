

jQuery(document).ready(function($) { 

if ($(".field-name-field-home-bottom-images").length) {
  $("article.node-home .field-collection-item-field-home-bottom-images .group-img").each(function() {
    $(this).css("min-height", function() {
      return $(this).find("img").attr("height");
    }).addClass("img-processed");
  });
  $(".field-name-field-home-bottom-images-image").each(function() {
    $(this).wrapInner('<div class="img-wrapper"></div>');
  });
  $(".field-name-field-home-bottom-images-link").each(function() {
    $(this).parent().find(".img-wrapper").wrapInner('<a href="'+ $(this).html().trim() +'"></a>');
  });
}

if($("body.page-faculty-research div.advanced-filters #edit-title-wrapper").length){
	$("body.page-faculty-research div.views-exposed-form #edit-keys-wrapper").before($("body.page-faculty-research div.advanced-filters #edit-title-wrapper"));
}


function Changetxt(obj) {
    if ($(obj).val().length > 0) {
        $(obj).removeClass('placeholder-on');
    }else{
	    	$(obj).addClass('placeholder-on');
    }
}



if($("html body.page-single-login").length){
	$("html body.page-single-login").parent().addClass('page-single-login');

	$("html body.page-single-login .form-item input").addClass('placeholder-on');

  $("html body.page-single-login .form-item input").keyup(function(){
	  Changetxt($(this));
  });
  

}

if($('div.view-directory-capstones .bef-select-as-links #edit-sort-by-0').length){
	$('div.view-directory-capstones .bef-select-as-links #edit-sort-by-0').html('<a href="' + location.pathname + '">View All</a>');
}

if($("section#section-content").length){
	$("section#section-content").before('<header class="print-logo"><img src="/sites/all/themes/sipa/images/sipalogoblack.png" alt="logo" title="logo"></header>');	
}

if($("body.page-node-18007 p a").length){

var lastHash = location.hash;                 //set it initially
$(window).bind('hashchange', function(event){
   lastHash = location.hash;                  //update it
   scrollToAnchor(lastHash);
});
   
    
var storedHash = window.location.hash;
	scrollToAnchor(storedHash);
}


if($('.view-directory-faculty div.group-left div.hide-image').length){
	$('.view-directory-faculty div.group-left div.hide-image').each(function(){
		$(this).parent().parent().removeClass('image');	
	});
}

if($('.advanced-filters .views-submit-button').length && $('.views-exposed-widgets .views-reset-button').length){
	$('.advanced-filters .views-submit-button').after($('.views-exposed-widgets .views-reset-button').clone());
	$('.advanced-filters .views-reset-button').attr('id','reset-button-advanced');
}

/*********************** Header Search *******/
	
	$('a.trigger').hover(function(e) {
		    e.stopPropagation();
		    $('#google-appliance-block-form').stop(true).animate({"marginRight": "0px"}, "200");
		}).bind("click", function() {
		  $('#google-appliance-block-form .form-submit').click();
		});
		
  $('#google-appliance-block-form input[name="search_keys"]').bind("blur", function() {
    if(this.value=='') {this.value='Search';}
  }).bind("focus", function() {
    if(this.value=='Search') {this.value='';}
  }).blur();
		
/*
	$('#zone-user').mouseleave(function(){
	    $(this).find('#search-header-form').stop(true).animate({"marginRight": "-178px"}, "200");
	});
*/
	
	
/********************** Menu Hover *********************/	
/*
	$("header#section-header ul.menu li:not(ul.menu li ul.menu li)").hover(function(){
		$(this).find('ul.menu').fadeToggle("fast");
	});
*/
	
	var settings = {
    	sensitivity: 7,
    	interval: 50,
    	timeout: 100,
    	over: showNav,
    	out: hideNav
    }; 
	$("header#section-header ul.menu li:not(ul.menu li ul.menu li)").hoverIntent( settings );

function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid.replace('#','') +"']");
    $('html,body').animate({scrollTop: aTag.offset().top - 100},'slow');
}

	function showNav(){
		var navHeight = $(this).find('ul.menu');
		navHeight = navHeight.height();
		console.log(navHeight);
	    $(this).find('ul.menu').fadeIn("100");
	    $(this).addClass('hover');
	    $('html').addClass('menu-open');
	    $('.section-content').css('top', 'navHeight');
	}
	
	
	function hideNav() {
	
	    $(this).find('ul.menu').fadeOut("100");
	    $(this).removeClass('hover');
	    $('html').removeClass('menu-open');
	
	}

/* Accordion Page Triggers */
$('body.node-type-accordion-page #region-content .field-name-field-item-title').click(function() {
	$(this).parent().find('.field-group-format-toggler a').trigger('click');
});

/* FAQ Page Triggers */
$('body.page-faq #region-content .group_section h2').click(function() {
	$(this).parent().parent().find('.field-group-format-toggler a').trigger('click');
});
		

/*********************** Hero Slider *******/
	$('.field-name-field-hero-item, .field-name-field-banner-slide').orbit({
	     animation: 'fade', // fade, horizontal-slide, vertical-slide, horizontal-push
	     animationSpeed: 800, // how fast animtions are
	     timer: true, // true or false to have the timer
	     resetTimerOnClick: false, // true resets the timer instead of pausing slideshow progress
	     advanceSpeed: 6000, // if timer is enabled, time between transitions
	     pauseOnHover: false, // if you hover pauses the slider
	     startClockOnMouseOut: false, // if clock should start on MouseOut
	     startClockOnMouseOutAfter: 1000, // how long after MouseOut should the timer start again
	     directionalNav: false, // manual advancing directional navs
	     captions: false, // do you want captions?
	     captionAnimation: 'fade', // fade, slideOpen, none
	     captionAnimationSpeed: 800, // if so how quickly should they animate in
	     bullets: true,	// true or false to activate the bullet navigation
	     bulletThumbs: false,	// thumbnails for the bullets
	     bulletThumbLocation: '',	// location from this file where thumbs will be
	     afterSlideChange: function(){}, // empty function
	     fluid: true // or set a aspect ratio for content slides (ex: '4x3')
	});
	
	
/*********************** Apply even class to rows *******/
	$("body.node-type-program-page #region-content .field-collection-container .field-collection-view:even").addClass('even');
	
	$("body.node-type-field-of-study #region-content .field-collection-container .field-collection-view:even").addClass('even');
	
	$("body.node-type-hub-page #region-content .field-collection-container .field-collection-view:even").addClass('even');
	   
/*********************** Apply even first class to top row of Calendar *******/	
	$('.calendar-calendar .month-view tbody tr:first-child').addClass('first-row');


/*********************** Input Placeholders *******/	
	  $('#edit-keys, #edit-search').watermark('Enter search terms');
	  if($('body.page-faculty-research #edit-keys').length){
	  	$('body.page-faculty-research #edit-keys').watermark('Enter keyword');
	  }
	  if($('body.page-faculty-research div.views-exposed-form #edit-title').length){
	  	$('body.page-faculty-research div.views-exposed-form #edit-title').watermark('Enter faculty name');
	  }
	 
	  $("#block-block-11 input, #edit-keys, #edit-search").each(
            function(){
                $(this).data('holder',$(this).attr('placeholder'));
                $(this).focusin(function(){
                    $(this).attr('placeholder','');
                });
                $(this).focusout(function(){
                    $(this).attr('placeholder',$(this).data('holder'));
                });
                
        });

/*********************** Sipa Selector *******/
	//make the tabs layer under each other left to right for shadows
	var zIndexNumber = 40;
	$(".horizontal-tab-button, .view_tabs .view_tab, .switcher-tabs li").each(function() {
		$(this).css('zIndex', zIndexNumber);
		zIndexNumber -= 1;
	});


/*********************** Gallery *******/
	//unwrap the photogallery field collection view to prepare for slideshow styling
	//no template available to remove this div - hardcoded in the field collection module
	$(".node-photo-gallery .showcase-content").unwrap();

	//photo gallery slideshow
	//https://bitbucket.org/awkwardgroup/awkward-showcase/wiki/Home
	$(".node-photo-gallery .field-collection-container").awShowcase({
		thumbnails: true,
		transition: 'fade',
		thumbnails_direction: 'vertical',
		thumbnails_slidex: 1,
		arrows: true,
		buttons: false,
		/* content_height:	496, */
		content_width: 468,
		dynamic_height: true,
		viewline: false, /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of images in the source. */
		custom_function: slide_counter //update slide counter
	});
	
	
	
	
	
	function galleryHeight(){  
	div = $('.field-name-field-photo-gallery-slide-desc');
	maxHeight = 0;
	for(i=0; i<div.length; i++){
	    if(maxHeight < $(div[i]).height()){
	      maxHeight = $(div[i]).height();
	    }
	  }
	  $('.field-name-field-photo-gallery-slide-desc').css('height', maxHeight+'px');
	  $('.field-name-degree-conentration-teasers .view-content .views-row').hide();
	  $('.group_policy_concentration .field-group-format-toggler a').trigger('click');
	}

	
	

/********************** Shadowbox *********************/
 	Shadowbox.init({
  	  initialHeight: 300,
  	  initialWidth: 300,
  	  height: 800,
  	  width: 800
  	});         

       $('.field-name-photo-gallery-popup a').live('click', function() {
                Shadowbox.open(this);
                return false;
        }); 
            
/********************** Sharing Links *********************/
	
	$('.no-banner #block-sharethis-sharethis-block').scrollToFixed({
    	marginTop: $('header').outerHeight(),
    	zIndex: 9
    });	
	
    $('.banner #block-sharethis-sharethis-block').scrollToFixed({
    	marginTop: $('header').outerHeight(),
    	zIndex: 9
    });
    	
	

/********************** Sidebar Slider Top 2 *********************/

     jQuery('.sidebar-scroll.view-latest-news .view-content, .sidebar-scroll.view-featured-events .view-content').each(function(){
	    var p = this.parentNode;
	    jQuery(this).cycle({
	      timeout:  0,
	      speed: 250,
	      fx:     'scrollHorz',
	      next: jQuery(this).parent().find('#next'), 
	      prev: jQuery(this).parent().find('#prev'),
	      pager:  jQuery('#nav', p)
	      
	    });    
	});
		
/********************** Admissions *********************/


	$('#prepare').click(function() {
	   $.scrollTo($('.group-prepare-to-apply'), 500, {offset:-120});
	   $(this).parent().parent().find('.active').removeClass('active');
	   $(this).addClass('active');
	   $('.group-prepare-to-apply.collapsed .field-group-format-toggler a').trigger('click');
	});
	
	$('#ready').click(function() {
	   $.scrollTo($('.group-applying-to-sipa'), 500, {offset:-120});
	   $(this).parent().parent().find('.active').removeClass('active');
	   $(this).addClass('active');
	   $('.group-applying-to-sipa.collapsed .field-group-format-toggler a').trigger('click');

	});
	
	$('#after').click(function() {
	   $.scrollTo($('.group-after-applying'), 500, {offset:-120});
	   $(this).parent().parent().find('.active').removeClass('active');
	   $(this).addClass('active');
	   $('.group-after-applying.collapsed .field-group-format-toggler a').trigger('click');
	});




/********************** Directory View Summary spacing *********************/	
	summaryWidth = $(".results").width();
	$(".page-directory #region-content .view-header .views-widget-sort-by").css( "left",summaryWidth+"px");
	
	

/********************** Curriculum *********************/	
	
	$('.field-name-degree-concentration .view-degree-concentrations .view-content').children().click(function() {
	
	    // Get the index of the link that was clicked on
	    var index = $('.view-degree-concentrations .view-content').children().index(this);
	
	    // Hide all content except that corresponding to the clicked link
	    $('.field-name-degree-conentration-teasers .view-content').children().fadeOut("fast").eq(index).fadeIn("fast");              
	
	    // Remove active class from all links except that which was clicked        
	    $('.view-degree-concentrations .view-content').children().removeClass('active');
	    $(this).addClass('active');        
	
	});
	
	$('.field-name-specialization-titles .view-degree-specializations .view-content').children().click(function() {
	
	    // Get the index of the link that was clicked on
	    var index = $('.view-degree-specializations .view-content').children().index(this);
	
	    // Hide all content except that corresponding to the clicked link
	    $('.field-name-specialization-teaser .view-content').children().fadeOut("fast").eq(index).fadeIn("fast");             
	
	    // Remove active class from all links except that which was clicked        
	    $('.view-degree-specializations .view-content').children().removeClass('active');
	    $(this).addClass('active');        
	
	});
	

	function evenHeight(){  
	div = $('.field-name-degree-conentration-teasers .view-content .views-row');
	maxHeight = 0;
	for(i=0; i<div.length; i++){
	    if(maxHeight < $(div[i]).height()){
	      maxHeight = $(div[i]).height();
	    }
	  }
	  $('.field-name-degree-conentration-teasers').css('height', maxHeight+'px');
	  $('.field-name-degree-conentration-teasers .view-content .views-row').hide();
	  $('.group_policy_concentration .field-group-format-toggler a').trigger('click');
	}
	

	function evenHeightTwo(){  
	div = $('.field-name-specialization-teaser .view-content .views-row');
	maxHeight = 0;
	for(i=0; i<div.length; i++){
	    if(maxHeight < $(div[i]).height()){
	      maxHeight = $(div[i]).height();
	    }
	  }
	  $('.field-name-specialization-teaser').css('height', maxHeight+'px');
	  $('.field-name-specialization-teaser .view-content .views-row').hide();
	  $('.group_specialization .field-group-format-toggler a').trigger('click');
	}
	
	evenHeight();
	evenHeightTwo();

/********************** Tracks Tabs *********************/	
	$('.field-name-track-title .view-academic-track .view-content').children().click(function() {
	
	    // Get the index of the link that was clicked on
	    var index = $('.view-academic-track .view-content').children().index(this);
	
	    // Hide all content except that corresponding to the clicked link
	    $('.field-name-track-teaser .view-content').children().fadeOut("fast").eq(index).fadeIn("fast");             
	
	    // Remove active class from all links except that which was clicked        
	    $('.view-academic-track .view-content').children().removeClass('active');
	    $(this).addClass('active');        
	
	});

	/********************** Slide Counter *********************/	
	  function slide_counter(){
      	var total = $(".showcase-thumbnail-wrapper").children().length;
      	var current_thumb = $(".showcase-thumbnail-wrapper div.active");//thumbnail with active class
      	var current = $(".showcase-thumbnail-wrapper div").index(current_thumb)+1;//get index position of active thumb, add 1 for zero-based counting
      	$("#counter").text(current + " of " + total );
      }

      slide_counter();

/********************** Sidebar position *********************/



	function sidebar_fix() {
	
		var sidebar = jQuery('.region-sidebar-second .region-inner');
		var sidebarHeight = sidebar.outerHeight(); 
		var contentHeight = jQuery('#region-content').height();
		var contentHeight2 = contentHeight + 100; 
		

	    if ($(window).height()-100 > sidebarHeight) {
	         $('.region-sidebar-second .region-inner').css({
		         position: 'fixed',//Keep it as is
		         top: 100
	         }); 
	    } 
	    
	    else if ($('#region-content').height() < sidebarHeight) {
	         $('.region-sidebar-second .region-inner').css({
		         position: 'absolute',//Scroll with the page
		         top: 0
	         }); 
	    } 
	    
	    else {
		    var bh = jQuery(window).height()-100;
		    var st = jQuery(window).scrollTop();
		    var el = jQuery('.region-sidebar-second .region-inner');
		    var eh = el.outerHeight();
		    if ( st >= (0 + eh) - bh ) {
		        //fix the positon and leave the green bar in the viewport
		        el.css({
		            position: 'fixed',
		            //left: el.offset().left,
		            bottom: 0
		        });
		    }
		    else {
		        // return element to normal flow
		        el.removeAttr("style");
		    }

	    }
	    
	    
	    	
	}
	
	jQuery(window).scroll(function() {
		sidebar_fix();
	});


	jQuery(window).resize(function() {	
		sidebar_fix();	
	});
	
/********************** Directory Accordion *********************/
	$('h3.director-trigger').click(function(){
		$(this).parent().parent().find('.advanced-filters').slideToggle(200);
		$(this).toggleClass('open');
	});	
	
	/* course directory advanced search needs to be open by default */
	if($("body.page-academics-courses #views-exposed-form-clone-of-directory-course-listings-page-1 div.toggler h3.director-trigger").length){
		$("body.page-academics-courses #views-exposed-form-clone-of-directory-course-listings-page-1 div.toggler h3.director-trigger").trigger("click");
	}

	
/********************** Course Directory Accordion *********************/
	$('.page-academics-courses.page-directory .view-content .field-group-format .toggler, .page-academics-courses.page-directory .view-content .group_course').live("click", function(){
		$(this).parent().parent().find('.course_content').slideToggle(200);
		$(this).parent().find('.toggler').toggleClass('open');
	});		
	
/********************** Form checkbox *********************/
	$(".node-type-application-checklist input:checkbox").uniform(); 
	
	
/********************** RSS Location Move *********************/	
	$("#region-content .feed-icon").appendTo("#region-content .rss-container");
	
	
/*********************** Exposed Form Select Menu Styling *******/	
	$(".views-exposed-form select, .form-select").selectbox({
		speed: 250
	});
  if ($(".form-item-field-academic-courses-tid .sbOptions").length) {
    $(".form-item-field-academic-courses-tid .sbOptions li").each(function() {
      if ($(this).has(":contains(---)").length) {
        $(this).addClass("level-4");
      }
      else if ($(this).has(":contains(--)").length) {
        $(this).addClass("level-3");
      }
      else if ($(this).has(":contains(-)").length && $(this).not(":contains(- Any -)").length) {
        $(this).addClass("level-2");
      }
      else {
        $(this).addClass("level-1");
      }
      $(this).not(".level-1").find("a").html( $(this).find("a").html().replace(/^---/, "++") );
      $(this).not(".level-1").find("a").html( $(this).find("a").html().replace(/^--/, "++") );
      $(this).not(".level-1").find("a").html( $(this).find("a").html().replace(/^-/, "+") );
    });
  }


/*********************** Break list into  *******/	
	$('#block-google-appliance-ga-related-searches ul').easyListSplitter({
	   colNumber: 3,
	   direction: 'horizontal'
	});	
	
	
/*********************** Scroll and Trigger for directory pages  *******/		
	if($("#block-system-main .view .view-summary:eq(0)").length){
		
	  var result_offset = $("#block-system-main .view .view-summary:eq(0)").offset().top - 125;
	  result_offset = (result_offset > 500) ? result_offset : 500;
	  
	
		if(window.location.href.indexOf("?") > -1 && window.location.href.indexOf("sort_by") != window.location.href.indexOf("?")+1) {
	/*  	       	$.scrollTo( '.group_top_info', 0, {onAfter: function(){sidebar_fix();}  	}); */
			$('html, body').animate({  scrollTop: result_offset}, 500);  
	    }
  }
    	
	if(window.location.href.indexOf("?"&&"&") > -1) {
		$("h3.director-trigger").toggleClass('open').parent().parent().find('.advanced-filters').show();
		
			if($("#block-system-main .view .view-summary:eq(0)").length){
					result_offset = $("#block-system-main .view .view-summary:eq(0)").offset().top - 125;
		    result_offset = (result_offset > 500) ? result_offset : 500;
		
		/* 		    $.scrollTo( '.group_top_info', 0, {onAfter: function(){sidebar_fix();}  	}); */
				$('html, body').animate({  scrollTop: result_offset}, 500);  
				
			}
    
  }
	


/*********************** Drupal Admin Gear Fix  *******/
	
	offset=13;
	
	$('.field-name-field-hero-item').children('.node-hero-item').each(function(i){
		var string_offset = offset + "px";
	    	$(this).find('.contextual-links-wrapper').css('left',string_offset);
	    offset = offset + 22;
	});




	


}); 	
	
	
	



jQuery(document).ajaxComplete(function() {
	//alert('Ajax complete');
	//refire select menu styling after AJAX
	jQuery(".views-exposed-form select").selectbox({
		speed: 250
	});
});







jQuery(window).load(function() {



	jQuery("aside .controller").fadeIn(250);
	
	jQuery(".orbit").fadeIn(250);

/********************** Sidebar Slider Twitter *********************/
	
     jQuery('.sidebar-scroll.view-twitter-feeds .view-content').each(function(){
	    var p = this.parentNode;
	    jQuery(this).cycle({
	      timeout:  0,
	      speed: 250,
	      fx:     'scrollHorz',
	      next: jQuery(this).parent().find('#next'), 
	      prev: jQuery(this).parent().find('#prev'),
	      pager:  jQuery('#nav', p)
	      
	    });    
	});

	/**
	 * move anchored content down
	 *//*
	if(window.location.hash && jQuery('#zone-content-wrapper').length) {
	  var hh = location.hash;
    hh = hh.substring(1,hh.length);
    jQuery('a#active-anchor-link-content').removeAttr('id');
    jQuery('a[name=' + hh + ']').attr('id','active-anchor-link-content');
  }*/

});

/**
 * move anchored content down
 *//*
if ("onhashchange" in window) {
  jQuery(window).bind( 'hashchange', function(e) {
    var hh = location.hash;
    hh = hh.substring(1,hh.length);
    jQuery('a#active-anchor-link-content').removeAttr('id');
    jQuery('a[name=' + hh + ']').attr('id','active-anchor-link-content');
  });
}*/


