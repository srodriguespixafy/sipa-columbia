<?php

/**
 * Theme callback 
 */
function theme_view_all_pager($vars) {
  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];
  $quantity = $vars['quantity'];
  $options = $vars['options'];
  
  $label = (!empty($options['label'])) ? $options['label'] : 'View All';
  
  if ($pager_page_array[$element] == 0) {  
     $li_next = theme('pager_next',
      array(
        'text' => (isset($tags[3]) ? $tags[3] : $label),
        'element' => $element,
        'interval' => 1,
        'parameters' => $parameters,
      )
    );

    if (empty($li_next)) {
      $li_next = "&nbsp;";
    }

   if ($li_next) {
      $items[] = array(
        'class' => array('view_all_pager'),
        'data' => $li_next,
      );
    }
    
    return theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}