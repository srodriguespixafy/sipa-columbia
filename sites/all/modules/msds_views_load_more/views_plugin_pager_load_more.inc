<?php

/**
 * The plugin to handle full pager.
 *
 * @ingroup views_pager_plugins
 */
class views_plugin_pager_load_more extends views_plugin_pager_full {
  function init(&$view, &$display, $options = array()) {
    parent::init($view, $display, $options);
  }

  function summary_title() {
    return "MSDS Load More";
    if (!empty($this->options['offset'])) {
      return format_plural($this->options['items_per_page'], 'Load more pager, @count item, skip @skip', 'Load more pager, @count items, skip @skip', array('@count' => $this->options['items_per_page'], '@skip' => $this->options['offset']));
    }
    return format_plural($this->options['items_per_page'], 'Load more pager, @count item', 'Load more pager, @count items', array('@count' => $this->options['items_per_page']));
  }

  function render($input) {
    global $base_url;
    $content_selector = '';
    $style_options = $this->view->style_options;
    $items_selector = '';
    switch ($this->view->plugin_name) {
      case 'default':
        $content_selector = 'div.view-content';
        $items_selector = '.views-row';
        break;
      case 'grid':
        $content_selector = 'div.view-content > table > tbody';
        $items_selector = 'tr';
        break;
      case 'list':
       if (array_key_exists('wrapper_class', $style_options) && !empty($style_options['wrapper_class'])) {
         $wrapper_class = '.' . $style_options['wrapper_class'];
       } else {
         $wrapper_class = '.item-list';
       }
       $content_selector = 'div.view-content>' . $wrapper_class . ' > *';
        $items_selector = '.views-row';
        break;
      case 'table':
        $content_selector = 'div.view-content > table > tbody';
        $items_selector = 'tr';
        break;
    }
    $pager_theme = views_theme_functions('msds_views_load_more_pager', $this->view, $this->display);
    return theme($pager_theme, array('tags' => $input, 'quantity' => $this->options['items_per_page'], 'view_name' => $this->view->name, 'current_display' => $this->view->current_display, 'content_selector' => $content_selector, 'items_selector' => $items_selector, 'element' => $this->options['id'], 'total_rows' => $this->view->total_rows));
  }
}
