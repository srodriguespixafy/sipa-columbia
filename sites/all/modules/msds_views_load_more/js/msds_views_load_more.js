// $Id:

(function ($) {
var msds_views_load_more_was_initialised = false;
Drupal.behaviors.msds_views_load_more = {
  attach:function() {
    // Make sure that autopager plugin is loaded
      if(!msds_views_load_more_was_initialised) {
        msds_views_load_more_was_initialised = true;
        // There should not be multiple Infinite Scroll Views on the same page
        if(Drupal.settings.msds_views_load_more.length > 0) { 
          var settings = Drupal.settings.msds_views_load_more[0];
          var use_ajax = false;
          // Make sure that views ajax is disabled
          if(Drupal.settings.views && Drupal.settings.views.ajaxViews) {
            $.each(Drupal.settings.views.ajaxViews, function(key, value) {
              if((value.view_name == settings.view_name) && (value.view_display_id == settings.display)) {
                use_ajax = true;
              }
            });
          }
          if(!use_ajax) {
            var view_selector    = 'div.view-id-' + settings.view_name + '.view-display-id-' + settings.display;
            var content_selector = view_selector + ' > ' + settings.content_selector;
            var items_selector   = content_selector + ' ' + settings.items_selector;
            var pager_selector   = view_selector + ' > div.item-list ' + settings.pager_selector;
            var next_selector    = view_selector + ' ' + settings.next_selector;
            var load_text_location     = view_selector + ' > div.view-content';
            var load_text         = '<div id="msds_views_load_more-ajax-loader" class="load-more"><div class="load_more_label">Load More</div></div>';
            var summary_selector  =   view_selector + ' .view-summary p.viewing';

            $(summary_selector).text('Viewing 1-'+$(items_selector).size());
            
            var control_code    = '<'
            $(pager_selector).hide();
            $(load_text_location).after(load_text);
            $('#msds_views_load_more-ajax-loader').click(function() {
              $.autopager('load');
            });
            
            var handle = $.autopager({
              autoLoad: false,
              appendTo: content_selector,
              content: items_selector,
              link: next_selector,
              page: 0,
              start: function() {
                $('body').append('<div class="ajax-progress"></div>');
                $('div.ajax-progress').append('<div class="throbber"></div>');
              },
              load: function() {
                Drupal.attachBehaviors(this);
                $('div.ajax-progress').remove();

                $(summary_selector).text('Viewing 1-'+$(items_selector).size());
                
                $(items_selector).removeClass('views-row-first');
                $(items_selector).removeClass('views-row-last');
                $(items_selector).first().addClass('views-row-first');
                $(items_selector).last().addClass('views-row-last');
                
                if ($(items_selector).size() == settings.total_rows) {
                  $('#msds_views_load_more-ajax-loader').remove();
                }
                
              }
              
            });
          }
          else {  
            alert(Drupal.t('Views infinite scroll pager is not compatible with Ajax Views. Please disable "Use Ajax" option.'));
          }
        }
        else if(Drupal.settings.msds_views_load_more.length > 1) {
          alert(Drupal.t('Views Infinite Scroll module can\'t handle more than one infinite view in the same page.'));
        }
      }
  }
}

})(jQuery);
