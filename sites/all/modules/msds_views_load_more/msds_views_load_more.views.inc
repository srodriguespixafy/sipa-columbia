<?php
/**
 * @file
 *  Provides the views plugin information.
 */

/**
 * Implements hook_views_plugin().
 */
function msds_views_load_more_views_plugins() {
  return array(
    'module' => 'msds_views_load_more',
    'pager' => array(
      'msds_load_more' => array(
        'title' => t('MSDS Load More'),
        'help' => t('msds_views_load_more'),
        'handler' => 'views_plugin_pager_load_more',
        'uses options' => TRUE,
        'parent' => 'full',
      ),
    ),
  );
}
