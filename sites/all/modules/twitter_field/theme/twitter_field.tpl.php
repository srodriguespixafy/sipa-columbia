<?php

/**
 * @file
 * Theme template for a list of tweets.
 *
 * Available variables in the theme include:
 *
 * 1) An array of $tweets, where each tweet object has:
 *   $tweet->id
 *   $tweet->username
 *   $tweet->userphoto
 *   $tweet->text
 *   $tweet->timestamp
 *   $tweet->time_ago
 *
 * 2) $twitkey string containing initial keyword.
 *
 * 3) $title
 *
 */
?>
  <?php if (is_array($tweets)): ?>
  <?php  $tweet = $tweets[0]; // only 1 tweet requested ?>
    
	<div class="tweet">
		<div class="twitter-date date-display-single"><?php print date('F j, Y - g:i A', $tweet->timestamp); ?></div>
		<div class="twitter-user-name views-field-title"><a href="http://twitter.com/<?php print urlencode($tweet->username);?> "><?php print $tweet->username; ?></a></div>
		<div class="twitter-message"><?php print twitter_pull_add_links($tweet->text); ?></div>
	</div>
		
<?php
  endif;
?>