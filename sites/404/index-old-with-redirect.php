<?php
header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
header("Status: 404 Not Found");
$url = htmlspecialchars($_SERVER["HTTP_HOST"]);
$seconds = 6;
header( 'Refresh: '.$seconds.'; url=http://'.$url );
?>
<!-- Fallback using meta refresh. -->
<html>
 <head>
  <title>Page not found!</title>
  <meta http-equiv="refresh" content="<?=$seconds?>;url=http://<?php echo $url; ?>">
  <style>
  body {margin: 0; padding: 0;font-family: arial;}
  .cu-name {
    background: url("/sites/404/crown.png") no-repeat scroll -5px 3px #253779;
    padding: 10px 10px 10px 30px;
    color:#FFFFFF;
  }
  .cu-name a {  color:#FFFFFF;}
  p, h1 {margin-left: 30px;}
  </style>
 </head>
 <body>
 <div class="cu-name"><a target="_blank" href="http://www.columbia.edu" class="ext">Columbia University in the City of New York</a></div>
 <h1>Page not found!</h1>
  <p>We're sorry, but the page you are looking for no longer exists at this location.</p>
  <p>You will be automatically redirected to the homepage in <?=$seconds?> seconds. If you aren't forwarded 
to the home page, <a href="http://<?php echo $url; ?>">click here</a>.
  </p>
 </body>
</html>