<?php
header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
header("Status: 404 Not Found");
$url = htmlspecialchars($_SERVER["HTTP_HOST"]);
$seconds = 6;
header( 'Refresh: '.$seconds.'; url=http://'.$url );
?>
<!-- Fallback using meta refresh. -->
<html>
 <head>
  <title>Page not found | Go Ask Alice!</title>
  <meta http-equiv="refresh" content="<?=$seconds?>;url=http://<?php echo $url; ?>">
  <style>body {margin: 0; padding: 0;font-family: arial;}</style>
 </head>
 <body>
  <a href="/" title="Home" rel="home" id="logo"><img src="/files/generic2/alice_logo.gif" alt="Home" /></a>
  <h1>Page not found!</h1>
  <p>We're sorry, but the page you are looking for no longer exists at this location.</p>
  <p>You will be automatically redirected to the homepage in <?=$seconds?> seconds. If you aren't forwarded 
to the home page, <a href="http://<?php echo $url; ?>">click here</a>.
  </p>
  <script type="text/javascript">
  <!--//--><![CDATA[//><!--
  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  //--><!]]>
  </script>
  <script type="text/javascript">
  <!--//--><![CDATA[//><!--
  var pageTracker = _gat._getTracker("UA-28392994-1");pageTracker._trackPageview();
  //--><!]]>
  </script>
 </body>
</html>