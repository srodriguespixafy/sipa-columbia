<?php
  header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
  header("Status: 404 Not Found");
  $url = htmlspecialchars($_SERVER["HTTP_HOST"]);
  $seconds = 6;

  $search_param = explode("/", $_SERVER['REQUEST_URI']);
  $search_param = array_pop($search_param);
?>
<!DOCTYPE HTML>
<html id="pagenotfound">

<head>
<title>Scheduled Maintenance!</title>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

<link type="text/css" rel="stylesheet" media="all" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />
<link type="text/css" rel="stylesheet" media="all" href="http://www.columbia.edu/sites/all/themes/ias/cu2m/theme.css" />
<link type="text/css" rel="stylesheet" media="all" href="/sites/404/404.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>

<script type="text/javascript" src="http://www.columbia.edu/sites/all/themes/ias/cu2m/theme.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>

<link rel="shortcut icon" href="http://www.columbia.edu/sites/all/themes/base/columbia2/images/favicon-crown.png" type="image/x-icon" />

</head>
<body id="top">

  <div id="" data-role="page" data-theme="d" class="page">

    <div data-role="header" class="region-header" data-theme="c">
      <div class="cu-name"><span>Columbia University in the City of New York</span></div>
    </div> <!-- /region-header -->
    
    <div data-role="content" data-theme="d" class="region-content">

      <div class="content-primary">
        <div class="region-content-area">
          <h1>Scheduled Maintenance!</h1>
          <p>Certain Columbia University websites are undergoing scheduled maintenance. We are sorry for the inconvenience and plan to have sites back up soon.</p>

          <ul data-role="listview" data-inset="true" data-divider-theme="b">
            <li data-role="list-divider">Please select from the following.</li>
            <li><a href="http://www.columbia.edu" rel="external">Go to Columbia.edu</a></li>
  
          </ul>
        </div>
      </div>

      <div class="content-secondary">
      </div>

    </div> <!-- /region-page -->

    <div data-role="footer" class="region-footer" data-theme="c">

      <div id="copyright">&copy; <?php print date("Y"); ?> The Trustees of Columbia University in the City of New York</div>
    </div> <!-- /region-footer -->

  </div> <!-- /region-page -->

</body>
</html>